package controllers

import (
	"finalproject/models"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LoginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
	Username    string `json:"username" binding:"required"`
	Password    string `json:"password" binding:"required"`
	Email       string `json:"email" binding:"required"`
	Nama        string `json:"nama" binding:"required"`
	NoHandphone string `json:"no_handphone" binding:"required"`
	Role        string `json:"role" binding:"required"`
}

// LoginUser godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body LoginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.Username = input.Username
	u.Password = input.Password

	token, err := models.LoginCheck(u.Username, u.Password, db)

	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	user := map[string]string{
		"username": u.Username,
	}

	c.JSON(http.StatusOK, gin.H{"message": "login success", "user": user, "token": token})

}

// Register godoc
// @Summary Register a user.
// @Description registering a user from public access.
// @Tags Auth
// @Param Body body RegisterInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input RegisterInput
	var checkUsername string
	var checkEmail string
	var checkRole int
	var userId int

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}
	up := models.UserProfile{}

	u.Username = input.Username
	u.Email = input.Email
	u.Password = input.Password
	up.Nama = input.Nama
	up.NoHandphone = input.NoHandphone
	up.Role = strings.ToLower(input.Role)

	data := map[string]string{
		"username":     input.Username,
		"email":        input.Email,
		"nama":         input.Nama,
		"no handphone": input.NoHandphone,
		"role":         input.Role,
	}

	db.Raw("SELECT username FROM users WHERE username = ?", input.Username).Row().Scan(&checkUsername)
	db.Raw("SELECT email FROM users WHERE email = ?", input.Email).Row().Scan(&checkEmail)
	db.Raw("SELECT COUNT(role) FROM user_profiles WHERE role LIKE 'admin'").Row().Scan(&checkRole)

	if input.Username == checkUsername {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"Status": "error", "reason": "Username telah terdaftar", "data": data})
		return
	} else if input.Email == checkEmail {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"Status": "error", "reason": "Email telah terdaftar", "data": data})
		return
	} else if strings.ToLower(input.Role) == "admin" && checkRole > 0 {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"Status": "error", "reason": "Admin hanya bisa daftar 1 kali", "data": data})
		return
	}

	_, err := u.SaveUser(db)

	db.Raw("SELECT id FROM users WHERE email = ?", input.Email).Row().Scan(&userId)
	up.UserId = uint(userId)
	fmt.Println("userid = ", userId)
	db.Create(&up)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Status": "registration success", "user": data})

}

package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type categoryInput struct {
	CategoryName string `json:"category_name"`
}

// GetCategory godoc
// @Summary Get Category
// @Description Get Category (dapat diakses oleh siapapun)
// @Tags Category
// @Produce json
// @Success 200 {object} models.Category
// @Router /category [get]
func GetCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var category []models.Category
	db.Find(&category)

	c.JSON(http.StatusOK, gin.H{"data": category})
}

// CreateCategory godoc
// @Summary Create New Category
// @Description Membuat Category baru (hanya bisa dilakukan oleh admin)
// @Tags Category
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body categoryInput true "the body to create a new category"
// @Produce json
// @Success 200 {object} models.Category
// @Router /category [post]
func CreateCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "admin" {
		var input categoryInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		category := models.Category{CategoryName: input.CategoryName}
		db.Create(&category)

		c.JSON(http.StatusOK, gin.H{"data": category})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya admin yang dapat create category"})
		return
	}

}

// UpdateCategory godoc
// @Summary Update Category
// @Description Update Category by id (hanya bisa di akses oleh admin)
// @Tags Category
// @Param id path string true "category id"
// @Param Body body categoryInput true "the body to update category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Category
// @Router /category/{id} [put]
func UpdateCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "admin" {
		var category models.Category
		if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		var input categoryInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var updatedInput models.Category
		updatedInput.CategoryName = input.CategoryName
		updatedInput.UpdatedAt = time.Now()

		db.Model(&category).Updates(updatedInput)

		c.JSON(http.StatusOK, gin.H{"data": category})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya admin yang dapat update category"})
		return
	}

}

// DeleteCategory godoc
// @Summary Delete one Category
// @Description Delete category by id (hanya bisa di akses oleh admin)
// @Tags Category
// @Param id path string true "category id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /category/{id} [delete]
func DeleteCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "admin" {
		var category models.Category
		if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		db.Delete(&category)

		c.JSON(http.StatusOK, gin.H{"data": true})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya admin yang dapat delete category"})
		return
	}

}

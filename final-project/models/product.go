package models

import (
	"time"
)

type (
	Product struct {
		ID          uint      `gorm:"primary_key" json:"id"`
		Sku         string    `json:"sku"`
		Name        string    `json:"name"`
		Description string    `json:"description"`
		Price       uint      `json:"price"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		UserId      uint      `json:"user_id"`
		CategoryId  uint      `json:"category_id"`
		User        User      `json:"-"`
		Category    Category  `json:"-"`
		Orders      []Orders  `json:"-"`
		Cart        []Cart    `json:"-"`
	}
)

package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ResultOrderDetail struct {
	ID              uint
	Subtotal        uint
	ShippingAddress string
	Kurir           string
	NoResi          string
	OrderStatus     string
	OrderDate       time.Time
	CreatedAt       time.Time
	UpdatedAt       time.Time
}

type ResiInput struct {
	OrderDetailId uint   `json:"order_detail_id" binding:"required"`
	Resi          string `json:"resi" binding:"required"`
}

type OrderStatusInput struct {
	OrderDetailId uint   `json:"order_detail_id" binding:"required"`
	OrderStatus   string `json:"order_status" binding:"required"`
}

// GetOrderDetail godoc
// @Summary Get OrderDetail
// @Tags Order Detail
// @Description Get Order Detail milik customer (hanya dapat diakses oleh customer)
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.OrderDetail
// @Router /order-detail [get]
func GetOrderDetail(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)
	var orderDetailID []uint
	var result []ResultOrderDetail
	db.Table("orders").Select("order_detail_id").Where("user_id = ?", tokenUserID).Scan(&orderDetailID)

	if err1 != nil || err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		db.Table("order_details").Where("id in ?", orderDetailID).Scan(&result)
		c.JSON(http.StatusOK, gin.H{"data": result})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat melihat order detail"})
		return
	}
}

// UpdateResi godoc
// @Summary Update Resi
// @Description Update Resi (hanya bisa di akses oleh toko)
// @Tags Order Detail
// @Param Body body ResiInput true "the body to update resi"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.OrderDetail
// @Router /add-shipping [put]
func UpdateResi(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "toko" {
		var orderDetail models.OrderDetail
		var input ResiInput

		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		if err := db.Where("id = ?", input.OrderDetailId).First(&orderDetail).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		var updatedInput models.OrderDetail
		updatedInput.NoResi = input.Resi
		updatedInput.UpdatedAt = time.Now()

		db.Model(&orderDetail).Updates(updatedInput)

		c.JSON(http.StatusOK, gin.H{"data": orderDetail})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya toko yang dapat update resi"})
		return
	}

}

// UpdateOrderStatus godoc
// @Summary Update Order Status
// @Description Update Order Status (hanya bisa di akses oleh kurir atau admin) akses kurir untuk mengubah order_status menjadi DELIVERED, akses admin untuk mengubah order_status menjadi PAID
// @Tags Order Detail
// @Param Body body OrderStatusInput true "the body to update order status"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.OrderDetail
// @Router /edit-order-status [put]
func UpdateOrderStatus(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "kurir" || tokenRole == "admin" {
		var orderDetail models.OrderDetail
		var input OrderStatusInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		if err := db.Where("id = ?", input.OrderDetailId).First(&orderDetail).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		var updatedInput models.OrderDetail
		updatedInput.OrderStatus = input.OrderStatus
		updatedInput.UpdatedAt = time.Now()

		db.Model(&orderDetail).Updates(updatedInput)

		c.JSON(http.StatusOK, gin.H{"data": orderDetail})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya admin atau kurir yang dapat update order status"})
		return
	}

}

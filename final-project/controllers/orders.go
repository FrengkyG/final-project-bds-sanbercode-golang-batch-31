package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CheckoutInput struct {
	ShippingAddress string `json:"shipping_address" binding:"required"`
	Kurir           string `json:"kurir" binding:"required"`
}

type ResultGetCart struct {
	ID           uint
	Qty          uint
	Price        uint
	TotalPerItem uint
	ProductId    uint
}

// Checkout godoc
// @Summary Checkout
// @Description Checkout order dari cart ke table orders dan order_details (hanya bisa dilakukan oleh customer)
// @Tags Checkout
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body CheckoutInput true "the body to checkout cart"
// @Produce json
// @Success 200 {object} models.OrderDetail
// @Router /checkout [post]
func Checkout(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var subtotal uint

	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	if err1 != nil || err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var input CheckoutInput

		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		var result []ResultGetCart
		db.Table("carts").Select("id", "qty", "price", "total_per_item", "product_id").Where("user_id = ? AND status LIKE ?", tokenUserID, "ACTIVE").Scan(&result)
		fmt.Println("TEST GET CARTS DULU")
		fmt.Println(&result)

		for i := 0; i < len(result); i++ {
			subtotal += result[i].TotalPerItem
		}
		orderDetail := models.OrderDetail{Subtotal: subtotal, ShippingAddress: input.ShippingAddress, Kurir: input.Kurir, NoResi: "WAITING RESI", OrderStatus: "WAITING PAYMENT", OrderDate: time.Now()}
		db.Create(&orderDetail)
		db.Table("carts").Where("user_id = ? AND status LIKE ?", tokenUserID, "ACTIVE").Update("status", "ORDERED")

		for i := 0; i < len(result); i++ {
			orders := models.Orders{OrderDetailId: orderDetail.ID, ProductId: result[i].ProductId, Qty: result[i].Qty, Price: result[i].Price, TotalPerItem: result[i].TotalPerItem, CartId: result[i].ID, UserId: tokenUserID}
			db.Create(&orders)
		}

		c.JSON(http.StatusOK, gin.H{"data": orderDetail})

	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat melakukan checkout"})
		return
	}

}

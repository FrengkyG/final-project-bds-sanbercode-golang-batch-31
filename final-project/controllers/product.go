package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ProductInput struct {
	Sku         string `json:"sku" binding:"required"`
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
	Price       uint   `json:"price" binding:"required"`
	CategoryID  uint   `json:"category_id" binding:"required"`
}

// GetProduct godoc
// @Summary Get Product
// @Description Get Product (dapat diakses oleh siapapun)
// @Tags Product
// @Produce json
// @Success 200 {object} models.Product
// @Router /product [get]
func GetProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var product []models.Product
	db.Find(&product)

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetProductByCategoryID godoc
// @Summary Get Category
// @Description Get Category (dapat diakses oleh siapapun)
// @Param id path string true "category id"
// @Tags Product
// @Produce json
// @Success 200 {object} models.Product
// @Router /product/{id}/category [get]
func GetProductByCategoryID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var product []models.Product
	if err := db.Where("category_id = ?", c.Param("id")).Find(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// CreateProduct godoc
// @Summary Create Product
// @Description create new product (hanya bisa diakses oleh toko)
// @Tags Product
// @Param Body body ProductInput true "the body to create product"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Product
// @Router /product [post]
func CreateProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	if err1 != nil || err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "toko" {
		var input ProductInput
		var checkCategory uint

		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		db.Table("categories").Select("id").Where("id = ?", input.CategoryID).Scan(&checkCategory)

		if input.CategoryID != checkCategory {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Category ID tidak ditemukan"})
			return
		}

		product := models.Product{Sku: input.Sku, Name: input.Name, Description: input.Description, Price: input.Price, UserId: tokenUserID, CategoryId: input.CategoryID}
		db.Create(&product)

		c.JSON(http.StatusOK, gin.H{"data": product})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya toko yang dapat create product"})
		return
	}

}

// UpdateProduct godoc
// @Summary Update product
// @Description Update product by id (hanya bisa di akses oleh toko)
// @Tags Product
// @Param id path string true "product id"
// @Param Body body ProductInput true "the body to update product"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Product
// @Router /product/{id} [put]
func UpdateProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "toko" {
		var product models.Product

		if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		var input ProductInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var checkCategory uint
		db.Table("categories").Select("id").Where("id = ?", input.CategoryID).Scan(&checkCategory)

		if input.CategoryID != checkCategory {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Category ID tidak ditemukan"})
			return
		}

		var updatedInput models.Product
		updatedInput.Sku = input.Sku
		updatedInput.Name = input.Name
		updatedInput.Description = input.Description
		updatedInput.Price = input.Price
		updatedInput.CategoryId = input.CategoryID
		updatedInput.UpdatedAt = time.Now()

		db.Model(&product).Updates(updatedInput)

		c.JSON(http.StatusOK, gin.H{"data": product})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya toko yang dapat update product"})
		return
	}

}

// DeleteProduct godoc
// @Summary Delete one Product
// @Description Delete product by id (hanya bisa di akses oleh toko)
// @Tags Product
// @Param id path string true "product id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /product/{id} [delete]
func DeleteProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "toko" {
		var product models.Product
		if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		db.Delete(&product)

		c.JSON(http.StatusOK, gin.H{"data": true})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya toko yang dapat delete product"})
		return
	}

}

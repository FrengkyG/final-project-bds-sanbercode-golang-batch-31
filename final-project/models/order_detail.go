package models

import (
	"time"
)

type (
	OrderDetail struct {
		ID              uint      `gorm:"primary_key" json:"id"`
		Subtotal        uint      `json:"subtotal"`
		ShippingAddress string    `json:"shipping_address"`
		Kurir           string    `json:"kurir"`
		NoResi          string    `json:"no_resi"`
		OrderStatus     string    `json:"order_status"`
		OrderDate       time.Time `json:"order_date"`
		CreatedAt       time.Time `json:"created_at"`
		UpdatedAt       time.Time `json:"updated_at"`
		Orders          []Orders  `json:"-"`
	}
)

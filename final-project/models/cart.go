package models

import (
	"time"
)

type (
	Cart struct {
		ID           uint      `gorm:"primary_key" json:"id"`
		Qty          uint      `json:"qty"`
		Price        uint      `json:"price"`
		TotalPerItem uint      `json:"total_per_item"`
		Status       string    `json:"status"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		ProductId    uint      `json:"product_id"`
		UserId       uint      `json:"user_id"`
		Product      Product   `json:"-"`
		User         User      `json:"-"`
	}
)

package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type AddressInput struct {
	Alamat      string `json:"alamat" binding:"required"`
	Kota        string `json:"kota" binding:"required"`
	Kodepos     uint   `json:"kodepos" binding:"required"`
	NoHandphone string `json:"no_handphone" binding:"required"`
}

// GetAddress godoc
// @Summary Get Address Customer
// @Description Get Address Customer By user_profile_id customer (hanya bisa di akses oleh customer)
// @Tags Address
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Address
// @Router /address [get]
func GetAddress(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var address []models.Address
	var userProfileId uint
	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	if err1 != nil || err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {

		db.Table("user_profiles").Select("id").Where("user_id = ?", tokenUserID).Scan(&userProfileId)

		db.Find(&address).Where("user_profile_id = ?", userProfileId)

		c.JSON(http.StatusOK, gin.H{"data": address})

	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat melihat alamat"})
		return
	}

}

// CreateAddress godoc
// @Summary Create Address
// @Description create new address customer (hanya bisa diakses oleh customer)
// @Tags Address
// @Param Body body AddressInput true "the body to create Address"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Address
// @Router /address [post]
func CreateAddress(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var userProfileId uint
	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	if err != nil || err1 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var input AddressInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		db.Table("user_profiles").Select("id").Where("user_id = ?", tokenUserID).Scan(&userProfileId)

		address := models.Address{Alamat: input.Alamat, Kota: input.Kota, KodePos: input.Kodepos, NoHandphone: input.NoHandphone, UserProfileId: userProfileId}
		db.Create(&address)

		c.JSON(http.StatusOK, gin.H{"data": address})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat membuat alamat"})
		return
	}

}

// UpdateAddress godoc
// @Summary Update Address
// @Description Update address by id (hanya bisa di akses oleh customer)
// @Tags Address
// @Param id path string true "address id"
// @Param Body body AddressInput true "the body to update address"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Address
// @Router /address/{id} [put]
func UpdateAddress(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var userProfileId uint
	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	if err != nil || err1 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var address models.Address
		var input AddressInput
		db.Table("user_profiles").Select("id").Where("user_id = ?", tokenUserID).Scan(&userProfileId)

		if err := db.Where("id = ? AND user_profile_id = ?", c.Param("id"), userProfileId).First(&address).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var updatedInput models.Address
		updatedInput.Alamat = input.Alamat
		updatedInput.Kota = input.Kota
		updatedInput.KodePos = input.Kodepos
		updatedInput.NoHandphone = input.NoHandphone
		updatedInput.UpdatedAt = time.Now()

		db.Model(&address).Updates(updatedInput)

		c.JSON(http.StatusOK, gin.H{"data": address})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat mengubah alamat"})
		return
	}

}

// DeleteAddress godoc
// @Summary Delete one Address
// @Description Delete address by id (hanya bisa di akses oleh customer)
// @Tags Address
// @Param id path string true "address id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /address/{id} [delete]
func DeleteAddress(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var userProfileId uint
	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)
	db.Table("user_profiles").Select("id").Where("user_id = ?", tokenUserID).Scan(&userProfileId)

	if err != nil || err1 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var address models.Address
		if err := db.Where("id = ? AND user_profile_id = ?", c.Param("id"), userProfileId).First(&address).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		db.Delete(&address)

		c.JSON(http.StatusOK, gin.H{"data": true})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya admin yang dapat delete category"})
		return
	}

}

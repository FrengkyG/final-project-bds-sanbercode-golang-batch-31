package config

import (
	"finalproject/models"
	"finalproject/utils"
	"fmt"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDataBase() *gorm.DB {
	environment := utils.Getenv("ENVIRONMENT", "development")

	if environment == "production" {
		username := os.Getenv("DATABASE_USERNAME")
		password := os.Getenv("DATABASE_PASSWORD")
		host := os.Getenv("DATABASE_HOST")
		port := os.Getenv("DATABASE_PORT")
		database := os.Getenv("DATABASE_NAME")
		// production
		dsn := "host=" + host + " user=" + username + " password=" + password + " dbname=" + database + " port=" + port + " sslmode=require"
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		if err != nil {
			panic(err.Error())
		}

		db.AutoMigrate(&models.Address{}, &models.Cart{}, &models.Category{}, &models.OrderDetail{}, &models.Orders{}, &models.Product{}, &models.UserProfile{}, &models.User{})

		return db
	} else {
		// development
		username := "xwhxnffwikirxn"
		password := "55d1ef6b7cdfbd58718d45e1530faaf587cadf03fe4ab9d642882a5db968db0f"
		host := "ec2-54-157-15-228.compute-1.amazonaws.com:5432"
		database := "d82786u2u63bkn"
		// postgres: //xwhxnffwikirxn:55d1ef6b7cdfbd58718d45e1530faaf587cadf03fe4ab9d642882a5db968db0f@ec2-54-157-15-228.compute-1.amazonaws.com:5432/d82786u2u63bkn
		dsn := fmt.Sprintf("postgres://%v:%v@%v/%v", username, password, host, database)

		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

		if err != nil {
			panic(err.Error())
		}

		db.AutoMigrate(&models.Address{}, &models.Cart{}, &models.Category{}, &models.OrderDetail{}, &models.Orders{}, &models.Product{}, &models.UserProfile{}, &models.User{})

		return db
	}

}

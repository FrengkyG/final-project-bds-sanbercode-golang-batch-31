package models

type (
	Orders struct {
		OrderDetailId uint        `json:"order_detail_id"`
		ProductId     uint        `json:"product_id"`
		Qty           uint        `json:"qty"`
		Price         uint        `json:"price"`
		TotalPerItem  uint        `json:"total_per_item"`
		CartId        uint        `json:"cart_id"`
		UserId        uint        `json:"user_id"`
		OrderDetail   OrderDetail `json:"-"`
		Product       Product     `json:"-"`
		Cart          Cart        `json:"-"`
		User          User        `json:"-"`
	}
)

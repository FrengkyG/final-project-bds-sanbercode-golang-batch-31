package routes

import (
	"finalproject/controllers"
	"finalproject/middlewares"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/login", controllers.Login)
	r.POST("/register", controllers.Register)
	r.GET("/category", controllers.GetCategory)
	r.GET("/product", controllers.GetProduct)
	r.GET("/product/:id/category", controllers.GetProductByCategoryID)

	r.POST("/change-password", controllers.ChangePassword).Use(middlewares.JwtAuthMiddleware())
	r.PUT("/change-profile", controllers.ChangeProfile).Use(middlewares.JwtAuthMiddleware())

	r.POST("/category", controllers.CreateCategory).Use(middlewares.JwtAuthMiddleware())
	r.PUT("/category/:id", controllers.UpdateCategory).Use(middlewares.JwtAuthMiddleware())
	r.DELETE("/category/:id", controllers.DeleteCategory).Use(middlewares.JwtAuthMiddleware())

	r.POST("/product/", controllers.CreateProduct).Use(middlewares.JwtAuthMiddleware())
	r.PUT("/product/:id", controllers.UpdateProduct).Use(middlewares.JwtAuthMiddleware())
	r.DELETE("/product/:id", controllers.DeleteProduct).Use(middlewares.JwtAuthMiddleware())

	addressMiddlewareRoute := r.Group("/address")
	addressMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	addressMiddlewareRoute.GET("/", controllers.GetAddress)
	addressMiddlewareRoute.POST("/", controllers.CreateAddress)
	addressMiddlewareRoute.PUT("/:id", controllers.UpdateAddress)
	addressMiddlewareRoute.DELETE("/:id", controllers.DeleteAddress)

	cartMiddlewareRoute := r.Group("/cart")
	cartMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	cartMiddlewareRoute.GET("/", controllers.GetCart)
	cartMiddlewareRoute.POST("/", controllers.CreateCart)
	cartMiddlewareRoute.PUT("/:id", controllers.UpdateCart)
	cartMiddlewareRoute.DELETE("/:id", controllers.DeleteCart)

	r.POST("/checkout", controllers.Checkout).Use(middlewares.JwtAuthMiddleware())
	r.GET("/order-detail", controllers.GetOrderDetail).Use(middlewares.JwtAuthMiddleware())

	r.PUT("/add-shipping", controllers.UpdateResi).Use(middlewares.JwtAuthMiddleware())
	r.PUT("/edit-order-status", controllers.UpdateOrderStatus).Use(middlewares.JwtAuthMiddleware())

	return r
}

package models

import (
	"time"
)

type (
	Address struct {
		ID            uint        `gorm:"primary_key" json:"id"`
		Alamat        string      `json:"alamat"`
		Kota          string      `json:"kota"`
		KodePos       uint        `json:"kodepos"`
		NoHandphone   string      `json:"no_handphone"`
		CreatedAt     time.Time   `json:"created_at"`
		UpdatedAt     time.Time   `json:"updated_at"`
		UserProfileId uint        `json:"user_profile_id"`
		UserProfile   UserProfile `json:"-"`
	}
)

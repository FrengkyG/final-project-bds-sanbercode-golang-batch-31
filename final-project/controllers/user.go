package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type ChangePassInput struct {
	Password string `json:"password" binding:"required"`
}

// ChangePassword godoc
// @Summary Change Password
// @Description Change Password
// @Tags Auth
// @Param Body body ChangePassInput true "the body to change password"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /change-password [post]
func ChangePassword(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	token, err := token.ExtractTokenID(c)
	if err != nil {
		fmt.Println(err)
	}

	var user models.User
	var input ChangePassInput

	if err := db.Where("id = ?", token).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if errPassword != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.User
	updatedInput.Password = string(hashedPassword)
	updatedInput.UpdatedAt = time.Now()

	db.Model(&user).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"Status": "Change Password Success"})

}

package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ProfileInput struct {
	Nama        string `json:"nama" binding:"required"`
	NoHandphone string `json:"no_handphone" binding:"required"`
}

// ChangeProfile godoc
// @Summary Update Profile
// @Description Update Profil
// @Tags Auth
// @Param Body body ProfileInput true "the body to change profile"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.UserProfile
// @Router /change-profile [put]
func ChangeProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var userProfile models.UserProfile
	if err := db.Where("user_id = ?", tokenID).First(&userProfile).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input ProfileInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.UserProfile
	updatedInput.Nama = input.Nama
	updatedInput.NoHandphone = input.NoHandphone
	updatedInput.UpdatedAt = time.Now()

	db.Model(&userProfile).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": userProfile})
}

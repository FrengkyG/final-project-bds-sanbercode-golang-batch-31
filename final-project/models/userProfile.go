package models

import (
	"time"
)

type (
	UserProfile struct {
		ID          uint      `gorm:"primary_key" json:"id"`
		Nama        string    `json:"nama"`
		NoHandphone string    `json:"no_handphone"`
		Role        string    `json:"role"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		UserId      uint      `json:"user_id"`
		User        User      `json:"-"`
		Address     []Address `json:"-"`
	}
)

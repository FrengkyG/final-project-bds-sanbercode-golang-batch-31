package controllers

import (
	"finalproject/models"
	"finalproject/utils/token"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CartInput struct {
	Qty       uint `json:"qty" binding:"required"`
	ProductId uint `json:"product_id" binding:"required"`
}

// GetCart godoc
// @Summary Get Cart
// @Description Get Cart Customer (hanya bisa di akses oleh customer)
// @Tags Cart
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Cart
// @Router /cart [get]
func GetCart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var cart []models.Cart
	var cartId uint

	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	db.Raw("SELECT id FROM carts WHERE user_id = ? AND status LIKE ?", tokenUserID, "ACTIVE").Scan(&cartId)
	fmt.Println("Cart ID =", cartId)

	if err1 != nil || err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		if cartId == 0 {
			c.JSON(http.StatusOK, gin.H{"message": "Keranjang Belanja Anda Masih Kosong. Ayo segera masukan belanjaan Anda ke keranjang belanja!"})
			return
		} else {
			if err := db.Where("user_id = ?", tokenUserID).Where("status = ?", "ACTIVE").Find(&cart).Error; err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
				return
			}

			c.JSON(http.StatusOK, gin.H{"data": cart})
		}
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat melihat cart"})
		return
	}
}

// CreateCart godoc
// @Summary Create Cart
// @Description add to cart (hanya bisa diakses oleh customer)
// @Tags Cart
// @Param Body body CartInput true "the body to create Cart"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Cart
// @Router /cart [post]
func CreateCart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var checkProduct uint
	var price uint

	tokenRole, err1 := token.ExtractTokenRole(c)
	tokenUserID, err := token.ExtractTokenID(c)

	if err1 != nil || err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var input CartInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		db.Table("products").Select("id").Where("id = ?", input.ProductId).Scan(&checkProduct)
		db.Table("products").Select("price").Where("id = ?", input.ProductId).Scan(&price)

		if input.ProductId != checkProduct {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Product tidak ditemukan"})
			return
		}

		cart := models.Cart{Qty: input.Qty, Price: price, TotalPerItem: (input.Qty * price), Status: "ACTIVE", ProductId: input.ProductId, UserId: tokenUserID}
		db.Create(&cart)

		c.JSON(http.StatusOK, gin.H{"data": cart})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat add to cart"})
		return
	}

}

// UpdateCart godoc
// @Summary Update Qty Items in Cart
// @Description Update Qty of Product in Cart (hanya bisa di akses oleh customer)
// @Tags Cart
// @Param id path string true "cart id"
// @Param Body body CartInput true "the body to update qty items in cart"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Cart
// @Router /cart/{id} [put]
func UpdateCart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var cart models.Cart

		if err := db.Where("id = ?", c.Param("id")).First(&cart).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}

		var input CartInput
		var checkProduct uint
		var price uint

		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		db.Table("carts").Select("product_id").Where("id = ?", c.Param("id")).Scan(&checkProduct)
		db.Table("products").Select("price").Where("id = ?", input.ProductId).Scan(&price)

		if input.ProductId != checkProduct {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Product tidak boleh diubah, kecuali dihapus"})
			return
		}

		var updatedInput models.Cart
		updatedInput.Qty = input.Qty
		updatedInput.Price = price
		updatedInput.TotalPerItem = (input.Qty * price)
		updatedInput.UpdatedAt = time.Now()

		db.Model(&cart).Updates(updatedInput)

		c.JSON(http.StatusOK, gin.H{"data": cart})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat update cart"})
		return
	}

}

// DeleteCart godoc
// @Summary Remove product in cart
// @Description Remove product in cart (hanya bisa di akses oleh customer)
// @Tags Cart
// @Param id path string true "cart id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /cart/{id} [delete]
func DeleteCart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	tokenRole, err := token.ExtractTokenRole(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else if tokenRole == "customer" {
		var cart models.Cart
		if err := db.Where("id = ?", c.Param("id")).First(&cart).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
		}
		db.Delete(&cart)

		c.JSON(http.StatusOK, gin.H{"data": true})
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Hanya customer yang dapat delete cart"})
		return
	}

}
